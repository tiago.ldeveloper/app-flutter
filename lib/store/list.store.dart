import 'package:app/store/todo.store.dart';
import 'package:mobx/mobx.dart';

part 'list.store.g.dart';

class ListStore = _ListStore with _$ListStore;

abstract class _ListStore with Store {
  @observable
  String newTodoTitle = "";

  @action
  void setNewTodoTitle(String value) => newTodoTitle = value;

  @computed
  bool get isFormValid => newTodoTitle.isNotEmpty;

  ObservableList<TodoStore> todos = ObservableList();

  @action
  void add() {
    todos.insert(0, new TodoStore(newTodoTitle));
    newTodoTitle = "";
  }
}
