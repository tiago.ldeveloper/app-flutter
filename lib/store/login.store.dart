import 'package:mobx/mobx.dart';

part 'login.store.g.dart';

class LoginStore = _LoginStore with _$LoginStore;

abstract class _LoginStore with Store {
  @observable
  String email = "";

  @observable
  String password = "";

  @observable
  bool obscure = false;

  @observable
  bool loading = false;

  @observable
  bool logedIn = false;

  @action
  void setEmail(String value) => email = value;

  @action
  void setPassword(String value) => password = value;

  @action
  void setObscureTex() => obscure = !obscure;

  @computed
  bool get isEmailValid => email.length >= 6;

  @computed
  bool get isPasswordValid => password.length >= 6;

  @computed
  Function get loginOnPressed =>
      (isEmailValid && isPasswordValid && !loading) ? logar : null;

  Future<void> logar() async {
    loading = true;

    await Future.delayed(Duration(seconds: 1));

    loading = false;

    logedIn = true;

    email = "";
    password = "";
  }

  @action
  void logout() {
    logedIn = false;
  }
}
