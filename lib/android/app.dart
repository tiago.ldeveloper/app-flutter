import 'package:flutter/material.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.white,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        primaryColor: Colors.white,
      ),
      home: AppMaterial('Titulo'),
    );
  }
}

class AppMaterial extends StatefulWidget {

  final String titulo;
  AppMaterial(this.titulo);

  @override
  _AppMaterialState createState() => _AppMaterialState();
}

class _AppMaterialState extends State<AppMaterial> {

  double slider = 20;

  final List<Tab> listTabs =[
    Tab(child: Text(''),icon: Icon(Icons.add),text: 'dsdsad',),
    Tab(child: Text(''),icon: Icon(Icons.add),text: 'dsdsad',),
    Tab(child: Text(''),icon: Icon(Icons.add),text: 'dsdsad',),
  ];

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(title: Text('DSADASD'),),
      body: Container(
        margin: EdgeInsets.all(30),
        padding: EdgeInsets.all(8.0),
        child: Center(child: Column(
          children: [

            TabBar(
              
              tabs:  listTabs,
            ),

            Container(
              padding: EdgeInsets.all(10.0),
              margin: EdgeInsets.all(100),
              width: 300,
              height: 200,
              decoration: ShapeDecoration(
                shape: Border.all(
                  color: Colors.red,
                  width: 8.0
                ) + Border.all(
                  color: Colors.green,
                  width: 8.0
                ) + Border.all(
                  color: Colors.blue,
                  width: 8.0,
                )
              ),
            ),

            Slider(
              onChanged: (value){
                setState(() {
                  slider = value;
                });
              },
              value: slider,
              min: 0,
              max: 100,
              label: slider.round().toString(),
              divisions: 10,
            ),
            RaisedButton(
              child: Icon(Icons.cached),
              onPressed: () => showMyDialog(context),
            ),
            Text('SSSSS'),
            Divider(height: 10, color: Colors.black,),
            Text('SSSSS'),
            Divider(height: 10, color: Colors.black,),
            Text('SSSSS'),
            Divider(height: 10, color: Colors.black,),
            Card(
              elevation: 10,
              color: Colors.white54,
              shadowColor: Colors.white,
              child: Container(
                height: 400,
                width: 400,
                padding: EdgeInsets.all(10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Column(
                      children: [
                        Text('ASDAASDSA'),
                      ],
                    ),
                    Text('BBBBBBBBBB'),
                  ],
                ),
              ),
            ),

          ],
        )),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
      ),
    );
  }

  Future<void> showMyDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('AlertDialog Title'),
          shape: ContinuousRectangleBorder(
            borderRadius: BorderRadius.circular(20),
            side: BorderSide(
              width: 10.0,
              style: BorderStyle.solid,
              color: Colors.deepOrange
            )
          ),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('This is a demo alert dialog.'),
                Text('Would you like to approve of this message?'),
              ],
            ),
          ),
          actions: <Widget>[
            RaisedButton(
              child: Text('Approve'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}

