import 'package:app/android/drawer.dart';
import 'package:app/android/gridview.dart';
import 'package:app/android/popup.search.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final key = new GlobalKey<ScaffoldState>();
  final snack = const SnackBar(content: Text('Bem vindo!'));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: key,
      appBar: AppBar(
        elevation: 5,
        title: Text('Pesquisar contatos'),
        leading: IconButton(
          onPressed: () => key.currentState.openDrawer(),
          icon: Icon(Icons.menu),
        ),
        actions: actions(context),
        backgroundColor: Colors.white,
      ),
      drawer: DrawerApp(),
      body: Grid(),
    );
  }
  List<Widget> actions(context){
    List<Widget> actions = [
      IconButton(
        icon: Icon(Icons.search),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => PopupPesquisa(),
            ),
          );
        },
      ),
      PopupMenuButton(
        itemBuilder: (BuildContext context) => [
          const PopupMenuItem(
            value: 'Selecione',
            child: Text('ssssssssssssss'),
          ),
          const PopupMenuItem(
            value: 'WhyFarther.harder',
            child: Text('Working a lot harder'),
          ),
        ],
      ),
    ];
  }
}