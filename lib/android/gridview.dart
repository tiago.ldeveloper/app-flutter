import 'package:flutter/material.dart';

import 'package:flutter/material.dart';

class Grid extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: GridView.count(
        primary: false,
        padding: EdgeInsets.all(20.0),
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
        crossAxisCount: 2,
        children: [
          for (int index = 0; index <= 10; index++)
            GestureDetector(
              child: Container(
                padding: EdgeInsets.all(8.0),
                child: Text('Orange'),
                color: Colors.deepOrange,
              ),
              onTap: () {
              },
            ),
          Container(
            padding: EdgeInsets.all(8.0),
            child: Text('Orange'),
            color: Colors.deepOrange,
          ),
        ],
      ),
    );
  }
}
