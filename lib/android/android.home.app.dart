import 'package:app/android/drawer.dart';
import 'package:app/android/gridview.dart';
import 'package:app/android/home.dart';
import 'package:app/android/page.dart';
import 'package:app/android/popup.search.dart';
import 'package:flutter/material.dart';

class AndroidHomeApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.white,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        primaryColor: Colors.white,
      ),
      home: Home(),
    );
  }
}
