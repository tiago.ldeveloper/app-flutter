import 'package:app/android/android.home.app.dart';
import 'package:app/android/home.dart';
import 'package:flutter/material.dart';

class PopupPesquisa extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
            },
          ),
          title: Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              textAlign: TextAlign.start,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.zero,
                border: InputBorder.none,
                labelText: 'Pesquisar',
                hintText: 'Pesquisar',
                suffixIcon: Icon(Icons.search),
              ),
              onTap: () {},
            ),
          ),
        ),
        body: Scrollbar(
          child: ListView(
            children: [
              for (int index = 0; index <= 20; index++)
                ListTile(
                  title: Text('Laranja $index'),
                ),
            ],
          ),
        ),
      ),
    );
  }
}
