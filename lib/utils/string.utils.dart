import 'package:app/constantes/app.constantes.dart' ;
import 'package:app/utils/validador.util.dart';
import 'package:flutter/material.dart';


class AppUtil {

  static String validaCampo(String valor, String nomeCampo, TextInputType inputType, int tamanho, bool ehCampoObrigatorio) {

    if (ehCampoObrigatorio) {
      if (isBlank(valor)) {
        return nomeCampo + " é obrigatório.";
      }

      if (TextInputType.emailAddress == inputType &&
          isEmail(valor)) {
        return "E-mail inválido";
      }

      if ((inputType != TextInputType.emailAddress) &&   tamanho > 0 &&
          !ValidadorUtil.minLength(valor, tamanho)) {
        return nomeCampo + " deve ser maior que " +  tamanho.toString() +  " caracteres.";
      }
    }
    return null;
  }

  static String defaultString(String str, {String defaultStr = ''}) => str ?? defaultStr;
  static bool isBlank(String s) => (s == null || s.isEmpty) ? true : false;
  static bool isNotBlank(String s) => !isBlank(s);
  static bool equalsIgnoreCase(String a, String b) =>
      (a == null && b == null) ||
          (a != null && b != null && a.toLowerCase() == b.toLowerCase());

  static bool isEmail(String email) {
    RegExp regExp = new RegExp(AppConstantes.STRING_PATTERN);
    return isNotBlank(email) && !regExp.hasMatch(email);
  }
}